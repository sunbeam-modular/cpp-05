#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    // parameterized ctor
    Complex(int r, int i)
    {
        cout << "Inside Parameterized Ctor" << endl;
        this->real = r;
        this->imag = i;
    }

    void acceptComplex()
    {
        cout << "Enter the real and imag values = ";
        cin >> this->real >> this->imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1(10, 20);
    c1.printComplex();

    Complex c2(30, 40);
    c2.printComplex();
    return 0;
}