#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    // Paramterless Ctor
    Complex()
    {
        cout << "Inside Parameterless Ctor" << endl;
        this->real = 1;
        this->imag = 2;
    }

    void acceptComplex()
    {
        cout << "Enter the real and imag values = ";
        cin >> this->real >> this->imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1;
    Complex c2;
    // Complex c3;
    // Complex c4;
    // Complex c5;
    // Complex c6;
    // Complex c7;

    c1.printComplex();
    c2.printComplex();

    return 0;
}