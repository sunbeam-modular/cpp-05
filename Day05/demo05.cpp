#include <iostream>
using namespace std;
class Complex
{
private:
    const int real;
    int imag;

public:
    // Constructor Members Initializer List
    Complex() : real(1), imag(2)
    {
        cout << "Inside Parameterless ctor" << endl;
        // this->imag = 2; // ALLOWED
    }

    Complex(int real, int imag) : real(real), imag(imag)
    {
        cout << "Inside Parameterized ctor" << endl;
        // this->real = real; // NOT Allowed
        // this->imag = imag;
    }
    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1;
    c1.printComplex();

    Complex c2(10, 20);
    c2.printComplex();
    return 0;
}