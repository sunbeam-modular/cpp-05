#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    void acceptComplex()
    {
        cout << "Enter the real and imag values = ";
        cin >> this->real >> imag;
    }

    void printComplex()
    {
        cout << "Real = " << real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1;
    Complex c2;

    c1.acceptComplex();
    c2.printComplex();
    c1.printComplex();

    c2.acceptComplex();
    c2.printComplex();

    return 0;
}