#include <iostream>
using namespace std;

class Account
{
private:
    const int accno;
    long int adharno;
    double balance;

public:
    Account(int accno) : accno(accno)
    {
        // this->accno = accno;
        this->adharno = 0;
        this->balance = 0;
    }
    void transaction(bool status, double balance)
    {
        if (status)
            this->balance = this->balance + balance;
        else
            this->balance = this->balance - balance;
    }
    void printAccountDetails() const
    {
        cout << "Accno = " << this->accno << endl;
        cout << "Balance = " << this->balance << endl;
    }

    // Inspectors
    double getBalance() const
    {
        return this->balance;
    }

    long getAdhar() const
    {
        return this->adharno;
    }

    int getAccNo() const
    {
        return this->accno;
    }

    // Mutators
    void setAdharno(long adharno)
    {
        this->adharno = adharno;
    }
};

int main()
{
    int accno = 100;

    Account a1(++accno);
    a1.printAccountDetails();

    Account a2(++accno);
    a2.printAccountDetails();

    Account a3(++accno);
    a3.printAccountDetails();

    a1.transaction(true, 5000);
    cout << "Your current balance = " << a1.getBalance() << endl;

    a2.transaction(true, 3000);
    cout << "Your balance = " << a2.getBalance() << endl;

    a3.transaction(true, 7000);
    cout << "Balance = " << a3.getBalance() << endl;

    a1.updateAdhar(12345678);
    cout << "Updated Adahr = " << a1.getAdhar() << endl;

    return 0;
}