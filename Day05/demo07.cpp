#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    mutable int imag;

public:
    Complex(int real, int imag)
    {
        cout << "Inside Parameterized ctor" << endl;
        this->real = real;
        this->imag = imag;
    }
    void printComplex() const
    {
        // this->real = 30; // NOT OK
        this->imag = 40; // NOT OK
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1(10, 20);
    c1.printComplex();
    return 0;
}