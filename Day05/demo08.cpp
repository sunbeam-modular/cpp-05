#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int imag;

public:
    Complex()
    {
        this->real = 1;
        this->imag = 2;
    }

    void acceptComplex()
    {
        cout << "Enter the real and imag values = ";
        cin >> this->real >> this->imag;
    }

    void printComplex() const
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1;
    c1.acceptComplex();
    c1.printComplex();

    const Complex c2;
    // c2.acceptComplex(); // NOT OK
    c2.printComplex();
    return 0;
}