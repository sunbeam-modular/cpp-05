#include <iostream>
using namespace std;
// Constructor Overloading

class Complex
{
private:
    int real;
    int imag;

public:
    // Parameterless Ctor
    Complex()
    {
        cout << "Inside Parameteless Ctor" << endl;
        this->real = 1;
        this->imag = 2;
    }

    // parameterized ctor
    Complex(int real, int imag)
    {
        cout << "Inside Parameterized Ctor" << endl;
        this->real = real;
        this->imag = imag;
    }

    void acceptComplex()
    {
        cout << "Enter the real and imag values = ";
        cin >> this->real >> this->imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1(10, 20);
    c1.printComplex();

    Complex c2(30, 40);
    c2.printComplex();

    Complex c3;
    c3.printComplex();

    return 0;
}