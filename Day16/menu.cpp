#include <iostream>
using namespace std;
#include "menu.h"
#include "rectangle.h"
#include "circle.h"

void Menu::mainMenuList()
{
    cout << "-----------------------------------------" << endl;
    cout << "0.Exit" << endl;
    cout << "1.Add Shape & Calculate Area" << endl;
    cout << "2.Dispaly area of all shapes" << endl;
    cout << "3.Display specific shape area" << endl;
    cout << "Enter Choice = ";
    cin >> choice;
    cout << "-----------------------------------------" << endl;
}

void Menu::shapeMenuList()
{
    cout << "-----------------------------------------" << endl;
    cout << "0.Exit" << endl;
    cout << "1.Rectangle" << endl;
    cout << "2.Circle" << endl;
    cout << "Enter Choice = ";
    cin >> choice;
    cout << "-----------------------------------------" << endl;
}

void Menu::shapeMenu()
{
    Shape *shape = NULL;
    shapeMenuList();
    switch (this->choice)
    {
    case 0:
        break;
    case 1:
        shape = new Rectangle();
        break;
    case 2:
        shape = new Circle();
        break;
    default:
        cout << "Wrong Choice :(" << endl;
        break;
    }
    if (shape != NULL)
    {
        shape->accept();
        shape->calculateArea();
        this->shapeList.add(shape);
    }
}

void Menu::shapeAreaMenu()
{
    shapeMenuList();
    switch (this->choice)
    {
    case 0:
        break;
    case 1:
        this->shapeList.displaySpecific(typeid(Rectangle));
        break;
    case 2:
        this->shapeList.displaySpecific(typeid(Circle));
        break;
    default:
        cout << "Wrong Choice :(" << endl;
        break;
    }
}

Menu::Menu()
{
    this->choice = 1;
}

void Menu::mainMenu()
{
    while (this->choice != 0)
    {
        this->mainMenuList();
        switch (this->choice)
        {
        case 0:
            cout << "Thank You for using our App :)" << endl;
            break;
        case 1:
            this->shapeMenu();
            this->choice = 1;
            break;
        case 2:
            this->shapeList.display();
            break;
        case 3:
            this->shapeAreaMenu();
            this->choice = 1;
            break;
        default:
            cout << "Wrong Choice :(" << endl;
            break;
        }
    }
}