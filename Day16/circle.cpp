#include <iostream>
using namespace std;
#include "circle.h"

void Circle::accept()
{
    cout << "Enter radius ";
    cin >> this->radius;
}
void Circle::calculateArea()
{
    this->area = this->radius * this->radius * 3.14;
}
void Circle::display()
{
    cout << "Area of Circle = " << this->area << endl;
}
