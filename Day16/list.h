#ifndef LIST_H
#define LIST_H
#include <iostream>
using namespace std;

template <class T>
class List
{
private:
    T *list[10];
    int index;

public:
    List()
    {
        this->index = 0;
    }

    void add(T *element)
    {
        if (this->index < 10)
        {
            this->list[this->index] = element;
            this->index++;
        }
        else
            cout << "List is full" << endl;
    }

    void display()
    {
        for (int i = 0; i < this->index; i++)
            this->list[i]->display();
    }
    void displaySpecific(const type_info &type)
    {
        for (int i = 0; i < this->index; i++)
            if (typeid(*this->list[i]) == type)
                this->list[i]->display();
    }
    ~List()
    {
        for (int i = 0; i < this->index; i++)
            delete this->list[i];
    }
};

#endif