#ifndef MENU_H
#define MENU_H
#include "list.h"
#include "shape.h"

class Menu
{
private:
    int choice;
    List<Shape> shapeList;
    void mainMenuList();
    void shapeMenuList();
    void shapeMenu();
    void shapeAreaMenu();

public:
    Menu();
    void mainMenu();
};
#endif