#ifndef SHAPE_H
#define SHAPE_H
class Shape
{
protected:
    double area;

public:
    virtual void accept() = 0;
    virtual void calculateArea() = 0;
    virtual void display() = 0;
};
#endif