#include <iostream>
using namespace std;
template <class T>
class List
{
private:
    T *list[5];
    int index;

public:
    List()
    {
        this->index = 0;
    }
    void add(T element)
    {
        if (this->index < 5)
        {
            this->list[this->index] = new T();
            *this->list[this->index] = element;
            //*list[0] = element
            this->index++;
        }
        else
            cout << "List is full" << endl;
    }

    void display()
    {
        for (int i = 0; i < this->index; i++)
            cout << *this->list[i] << endl;
    }
    ~List()
    {
        for (int i = 0; i < this->index; i++)
            delete this->list[i];
    }
};

class Student
{
private:
    int rollno;
    string name;

public:
    Student()
    {
        this->rollno = 0;
        this->name = "";
    }
    Student(int rollno, string name)
    {
        this->rollno = rollno;
        this->name = name;
    }
    friend ostream &operator<<(ostream &out, Student &student);
};
ostream &operator<<(ostream &out, Student &student)
{
    out << "Rollno = " << student.rollno << endl;
    out << "Name = " << student.name << endl;
    return out;
}

int main()
{
    List<int> l1;
    l1.add(10);
    l1.add(20);
    l1.add(30);
    l1.add(40);
    l1.add(50);
    l1.display();
    cout << "--------------------" << endl;

    List<double> l2;
    l2.add(10.10);
    l2.add(20.20);
    l2.add(30.30);
    l2.add(40.40);
    l2.add(50.50);
    l2.display();
    cout << "--------------------" << endl;

    List<char> l3;
    l3.add('a');
    l3.add('b');
    l3.add('c');
    l3.add('d');
    l3.add('e');

    l3.display();

    cout << "--------------------" << endl;

    List<Student> l4;
    l4.add(Student(1, "rohan"));
    l4.add(Student(2, "nilesh"));
    l4.add(Student(3, "nitin"));
    l4.add(Student(4, "sarang"));
    l4.add(Student(5, "rahul"));

    l4.display();
    return 0;
}