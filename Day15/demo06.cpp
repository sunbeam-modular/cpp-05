#include <iostream>
using namespace std;

/*template <typename T>
void sum(T num1, T num2)
{
    cout << "Addition = " << num1 + num2 << endl;
}*/

template <typename X, typename Y>
void sum(X num1, Y num2)
{
    cout << "Addition = " << num1 + num2 << endl;
}
int main()
{
    sum(10, 20);
    sum(10.20, 20.30);
    sum(10, 20.30);
    return 0;
}