#include <iostream>
using namespace std;

class Derived
{
    int num1;

public:
    Derived()
    {
        this->num1 = 10;
    }
    void f2() const
    {
        // this->num1 = 50; // NOT allowed
        cout << "Derived::f2() = " << this->num1 << endl;
    }
    void f3() const //  Derived *const this
    {
        const_cast<Derived *>(this)->num1 = 100;
        cout << "Derived::f3() = " << this->num1 << endl;
    }
};

int main()
{
    Derived d1;
    d1.f2();
    d1.f3();
    return 0;
}