#include <iostream>
using namespace std;

class Base
{
public:
    void f1()
    {
        cout << "Base::f1()" << endl;
    }
    void f2()
    {
        cout << "Base::f2()" << endl;
    }
};

class Derived : public Base
{
    int num1;

public:
    Derived()
    {
        cout << "Inside Derived Ctor" << endl;
        this->num1 = 10;
    }
    void f2()
    {
        cout << "Derived::f2()" << endl;
    }
    void f3()
    {
        cout << "Derived::f3() = " << this->num1 << endl;
    }
};

int main()
{
    Base *bptr = new Base();
    bptr->f1();

    Derived *dptr = static_cast<Derived *>(bptr);
    if (dptr == NULL)
        cout << "static Cast has Failed" << endl;
    else
    {
        cout << "static Cast is successful" << endl;
        dptr->f3();
    }

    delete dptr;
    cout << "Code Completed" << endl;
    return 0;
}