#include <iostream>
using namespace std;

class Base
{
public:
    void f1()
    {
        cout << "Base::f1()" << endl;
    }
    virtual void f2()
    {
        cout << "Base::f2()" << endl;
    }
};

class Derived : public Base
{

public:
    void f3()
    {
        cout << "Derived::f3()" << endl;
    }
};

int main()
{
    Base b;
    Derived d;
    Base *bptr1 = new Base();
    Base *bptr2 = new Derived(); // upcasting

    Derived *dptr1 = new Derived();
    Derived *dptr2 = dynamic_cast<Derived *>(bptr2); // downcasting

    cout << "Type of b = " << typeid(b).name() << endl;
    cout << "Type of d = " << typeid(d).name() << endl;
    cout << "Type of bptr1 = " << typeid(bptr1).name() << endl;
    cout << "Type of bptr2 = " << typeid(bptr2).name() << endl;
    cout << "Type of *bptr1 = " << typeid(*bptr1).name() << endl;
    cout << "Type of *bptr2 = " << typeid(*bptr2).name() << endl;
    cout << "Type of dptr1 = " << typeid(dptr1).name() << endl;
    cout << "Type of dptr2 = " << typeid(dptr2).name() << endl;
    cout << "Type of *dptr1 = " << typeid(*dptr1).name() << endl;
    cout << "Type of *dptr2 = " << typeid(*dptr2).name() << endl;
    delete bptr1;
    delete bptr2;
    delete dptr1;

    return 0;
}