#include <iostream>
using namespace std;
template <class X, class Y>
class Complex
{
private:
    X real;
    Y imag;

public:
    Complex(X real, Y imag)
    {
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex<int, double> c1(10, 20.20);
    c1.printComplex();

    Complex<int, char> c2(10, 'B');
    c2.printComplex();

    Complex<char, double> c3('A', 20.10);
    c3.printComplex();
    return 0;
}