#include <iostream>
using namespace std;

class List
{
private:
    int *list[5];
    int index;

public:
    List()
    {
        this->index = 0;
    }
    void add(int element)
    {
        if (this->index < 5)
        {
            this->list[this->index] = new int();
            *this->list[this->index] = element;
            //*list[0] = element
            this->index++;
        }
        else
            cout << "List is full" << endl;
    }

    void display()
    {
        for (int i = 0; i < this->index; i++)
            cout << *this->list[i] << endl;
    }
    ~List()
    {
        for (int i = 0; i < this->index; i++)
            delete this->list[i];
    }
};

int main()
{
    List l1;
    l1.add(10);
    l1.add(20);
    l1.add(30);
    l1.add(40);
    l1.add(50);

    l1.display();
    return 0;
}