#include <iostream>
using namespace std;
template <class T>
class Complex
{
private:
    T real;
    T imag;

public:
    Complex(T real, T imag)
    {
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex<int> c1(10, 20);
    c1.printComplex();

    Complex<double> c2(10.20, 20.30);
    c2.printComplex();

    Complex<char> c3('A', 'B');
    c3.printComplex();
    return 0;
}