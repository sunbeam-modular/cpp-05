#include <iostream>
using namespace std;
// Multilevel Inheritance
class Base
{
private:
    int num1;

protected:
    int num2;

public:
    int num3;

    Base()
    {
        cout << "Base::Parameterless" << endl;
        this->num1 = 10;
        this->num2 = 20;
        this->num3 = 30;
    }

    void printBase()
    {
        cout << "Num1 = " << this->num1 << endl;
        cout << "Num2 = " << this->num2 << endl;
        cout << "Num3 = " << this->num3 << endl;
    }
    friend void myfun();
};

void myfun()
{
    Base b;
    cout << b.num1 << b.num2 << b.num3 << endl;
}

class Derived : Base
{
private:
    int num4;

public:
    Derived()
    {
        cout << "Derived::Parameterless" << endl;
        this->num4 = 40;
    }

    void printDerived()
    {
        cout << "Num4 = " << this->num4 << endl;
    }
};

class InDirect : Derived
{
private:
    int num5;

public:
    InDirect()
    {
        cout << "Indirect:;Parameterless" << endl;
        this->num5 = 50;
    }

    void printInDirect()
    {
        cout << "Num5 = " << this->num5 << endl;
    }
};

int main()
{
    Base b;
    cout << b.num1 << b.num2 << b.num3 << endl;

    Derived d;
    cout << d.num4 << endl;

    return 0;
}