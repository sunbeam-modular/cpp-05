#include <iostream>
using namespace std;
// Multiple Inheritance
// Hirerachical Inheritance
// Hybrid Inheritance
// Diamond Problem
class Base
{
private:
    int num1;

public:
    Base()
    {
        cout << "Inside Base::Parameterless ctor" << endl;
        this->num1 = 10;
    }
    Base(int num1)
    {
        cout << "Inside Base::Parameterized ctor" << endl;
        this->num1 = num1;
    }

    void printBase()
    {
        cout << "Num1 = " << this->num1 << endl;
    }

    ~Base()
    {
        cout << "Inside Base::Dtor" << endl;
    }
};

class Derived1 : public Base
{
private:
    int num2;

public:
    Derived1()
    {
        cout << "Inside Derived1::Parameterless ctor" << endl;
        this->num2 = 20;
    }

    Derived1(int num1, int num2) : Base(num1)
    {
        cout << "Inside Derived1::Parameterized ctor" << endl;
        this->num2 = num2;
    }

    void printDerived1()
    {
        printBase();
        cout << "Num2 = " << this->num2 << endl;
    }

    ~Derived1()
    {
        cout << "Inside Derived1::Dtor" << endl;
    }
};

class Derived2 : public Base
{
private:
    int num3;

public:
    Derived2()
    {
        cout << "Inside Derived2::Parameterless ctor" << endl;
        this->num3 = 30;
    }

    Derived2(int num1, int num3) : Base(num1)
    {
        cout << "Inside Derived2::Parameterized ctor" << endl;
        this->num3 = num3;
    }
    void printDerived2()
    {
        printBase();
        cout << "Num3 = " << this->num3 << endl;
    }

    ~Derived2()
    {
        cout << "Inside Derived2::Dtor" << endl;
    }
};

class Indirect : public Derived1, public Derived2
{
private:
    int num4;

public:
    Indirect()
    {
        cout << "Inside Indirect::Parameterless ctor" << endl;
        this->num4 = 40;
    }

    Indirect(int num1, int num2, int num3, int num4) : Derived1(num1, num2), Derived2(num1, num2), Base(num1)
    {
        cout << "Inside Indirect::Parameterized ctor" << endl;
        this->num4 = num4;
    }

    void printIndirect()
    {
        // printBase();
        printDerived1();
        printDerived2();
        cout << "Num4 = " << this->num4 << endl;
    }

    ~Indirect()
    {
        cout << "Inside Indirect::Dtor" << endl;
    }
};

int main()
{
    // Indirect i1;
    // i1.printIndirect();
    // Derived1 d1(100, 200);
    // d1.printDerived1();
    // Derived2 d2(100, 300);
    // d2.printDerived2();

    Indirect i1(100, 200, 300, 400);
    i1.printIndirect();

    return 0;
}