#include <iostream>
using namespace std;
// Single Inheritance

class Parent // Base
{
private:
    int num1;

protected:
    int num2;

public:
    int num3;

    Parent()
    {
        cout << "Parent::Parameterless" << endl;
        this->num1 = 10;
        this->num2 = 20;
        this->num3 = 30;
    }
    Parent(int num1, int num2, int num3)
    {
        cout << "Parent::Parameterized" << endl;
        this->num1 = num1;
        this->num2 = num2;
        this->num3 = num3;
    }

    void printParent()
    {
        cout << "Num1 = " << this->num1 << endl;
        cout << "Num2 = " << this->num2 << endl;
        cout << "Num3 = " << this->num3 << endl;
    }
    ~Parent()
    {
        cout << "Parent::Destructor" << endl;
    }
};

class Child : Parent // Derived
{
private:
    int num4;

public:
    Child()
    {
        cout << "Child::Parameterless" << endl;
        this->num4 = 40;
    }
    Child(int num1, int num2, int num3, int num4) : Parent(num1, num2, num3)
    {
        cout << "Child::Parameterized-2" << endl;
        this->num4 = num4;
    }
    Child(int num4)
    {
        cout << "Child::Parameterized" << endl;
        this->num4 = num4;
    }

    void printChild()
    {
        cout << "Num4 = " << this->num4 << endl;
    }
    ~Child()
    {
        cout << "Child::Destructor" << endl;
    }
};

int main()
{
    // Child c1;
    // Child c2(400);
    Child c3(100, 200, 300, 400);
    return 0;
}