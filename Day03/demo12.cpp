#include <iostream>
using namespace std;

inline int square(int num)
{
    int result = num * num;
    return result;
}

int main()
{
    int num;
    cout << "Enter num1 = ";
    cin >> num;
    int result = square(num);
    // At compile time
    // int result = num*num;
    cout << "Square of a number = " << result << endl;
    return 0;
}
