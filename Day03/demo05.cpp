#include <iostream>
namespace Na
{
    int num1 = 10;
    int num2 = 20;

    void f1()
    {
        printf("f1::Na\n");
    }

    struct Test1
    {
        void PrintTest1()
        {
            printf("PrintTest1::Na\n");
        }
    };
}

namespace Nb
{
    int num1 = 100;
    int num2 = 200;

    void f1()
    {
        printf("f1::Nb\n");
    }

    struct Test1
    {
        void PrintTest1()
        {
            printf("PrintTest1::Nb\n");
        }
    };
}

int main()
{
    Na::f1();
    Nb::f1();

    Na::Test1 t1;
    t1.PrintTest1();

    Nb::Test1 t2;
    t2.PrintTest1();
    return 0;
}