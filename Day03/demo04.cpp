#include <iostream>
namespace Na
{
    int num1 = 10;
    int num2 = 20;
}

namespace Nb
{
    int num1 = 100;
    int num2 = 200;
}

int main()
{
    printf("Value of num1 from Na Namespace =  %d\n", Na::num1);
    printf("Value of num2 from Na Namespace =  %d\n", Na::num2);

    printf("Value of num1 from Nb Namespace =  %d\n", Nb::num1);
    printf("Value of num2 from Nb Namespace =  %d\n", Nb::num2);

    return 0;
}