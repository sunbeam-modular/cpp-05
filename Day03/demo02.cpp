#include <iostream>

int num1 = 10;

namespace Na
{
    int num1 = 30;
}

int main()
{
    int num1 = 20;
    printf("Value of num1 from global scope =  %d\n", ::num1);
    printf("Value of num1 from local scope =  %d\n", num1);
    printf("Value of num1 from Na namespace =  %d\n", Na::num1);
    return 0;
}