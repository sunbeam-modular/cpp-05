#include <iostream>

int num1 = 10; // Global Namespace

int main()
{
    int num1 = 20;
    printf("Value of num1 from global scope =  %d\n", ::num1);
    printf("Value of num1 from local scope =  %d\n", num1);
    return 0;
}