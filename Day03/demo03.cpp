#include <iostream>

int num1 = 10;
int num2 = 20;

namespace Na
{
    int num1 = 100;
    int num2 = 200;
}

int main()
{
    int num1 = 1000;
    int num2 = 2000;
    printf("Value of num1 from global scope =  %d\n", ::num1);
    printf("Value of num2 from global scope =  %d\n", ::num2);
    printf("Value of num1 from Na Namespace =  %d\n", Na::num1);
    printf("Value of num2 from Na Namespace =  %d\n", Na::num2);

    return 0;
}