#include <iostream>
namespace Na
{
    int num1 = 10;
    int num2 = 20;

    void f1()
    {
        printf("f1::Na\n");
    }

    struct Test1
    {
        void PrintTest1()
        {
            printf("PrintTest1::Na\n");
        }
    };

    namespace Nb
    {
        int num1 = 100;
        int num2 = 200;

        void f1()
        {
            printf("f1::Nb\n");
        }

        struct Test1
        {
            void PrintTest1()
            {
                printf("PrintTest1::Nb\n");
            }
        };
    }
}

int main()
{
    printf("Value of num1 from nb namespace = %d\n", Na::Nb::num1);
    printf("Value of num2 from nb namespace = %d\n", Na::Nb::num2);

    Na::Nb::f1();

    Na::Nb::Test1 t1;
    t1.PrintTest1();
    return 0;
}