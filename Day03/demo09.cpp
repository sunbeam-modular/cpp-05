#include <iostream>

int main()
{
    int num1;
    int num2;
    // printf("Enter num1 & num2 = ");
    std::cout << "Enter num1 & num2 = ";

    // scanf("%d%d", &num1, &num2);
    std::cin >> num1 >> num2;

    // printf("Value  of num1 & num2 = %d , %d", num1, num2);
    std::cout << "Value  of num1 & num2 = " << num1 << " , " << num2;
    return 0;
}