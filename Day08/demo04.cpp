#include <iostream>
using namespace std;

void myfun()
{
    // local class
    class Complex
    {
    private:
        int real;
        int imag;

    public:
        Complex(int real = 10, int imag = 20)
        {
            this->real = real;
            this->imag = imag;
        }

        void printComplex()
        {
            cout << "Real = " << this->real << endl;
            cout << "Imag = " << this->imag << endl;
        }
    };

    Complex c1;
    c1.printComplex();

    Complex c2(100, 200);
    c2.printComplex();
}

int main()
{
    myfun();
    return 0;
}