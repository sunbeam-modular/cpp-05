#include <iostream>
using namespace std;

class Outer
{
private:
    int num1;

public:
    int num2;

    Outer(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }

    void printOuter()
    {
        cout << "Num1 = " << this->num1 << endl;
        cout << "Num2 = " << this->num2 << endl;
    }

    // Nested class
    class Inner
    {
    private:
        int num3;

    public:
        int num4;

        Inner(int num3, int num4)
        {
            this->num3 = num3;
            this->num4 = num4;
        }

        void printInner()
        {
            cout << "Num3 = " << this->num3 << endl;
            cout << "Num4 = " << this->num4 << endl;
        }
    };
};

int main()
{
    Outer o1(10, 20);
    o1.printOuter();
    o1.num2;
    Outer::Inner i1(20, 30);
    i1.printInner();
    i1.num4;
    return 0;
}