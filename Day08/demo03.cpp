#include <iostream>
using namespace std;
class Demo1
{
private:
    int num1;

protected:
    int num2;

public:
    int num3;

    Demo1(int num1 = 0, int num2 = 0, int num3 = 0)
    {
        this->num1 = num1;
        this->num2 = num2;
        this->num3 = num3;
    }
    void printDemo1()
    {
        cout << num1 << " , " << num2 << " , " << num3 << endl;
    }
    friend class Maths;
};

class Maths
{
public:
    void total()
    {
        Demo1 d1(10, 20, 30);
        cout << "Total of data members = " << d1.num1 + d1.num2 + d1.num3 << endl;
    }
    void avg()
    {
        Demo1 d1(10, 20, 30);
        cout << "Avg of data members = " << (d1.num1 + d1.num2 + d1.num3) / 3 << endl;
    }
};

int main()
{
    Maths m1;
    m1.total();
    m1.avg();
    return 0;
}