#include <iostream>
using namespace std;
class Demo
{
private:
    int num1;

protected:
    int num2;

public:
    int num3;
    // Demo()
    // {
    //     this->num1 = 0;
    //     this->num2 = 0;
    //     this->num3 = 0;
    // }

    Demo(int num1 = 0, int num2 = 0, int num3 = 0)
    {
        this->num1 = num1;
        this->num2 = num2;
        this->num3 = num3;
    }
    void printDemo()
    {
        cout << num1 << " , " << num2 << " , " << num3 << endl;
    }

    int getNum1()
    {
        return this->num1;
    }

    int getNum2()
    {
        return this->num2;
    }
    int getNum3()
    {
        return this->num3;
    }
    friend void sum(Demo &d1, Demo &d2);
    // friend Demo sum(Demo &d1, Demo &d2);
};

void sum(Demo &d1, Demo &d2)
{
    /*int num1 = d1.num1 + d2.num1;
    int num2 = d1.num2 + d2.num2;
    int num3 = d1.num3 + d2.num3;
    Demo d3(num1, num2, num3);
    d3.printDemo();*/

    Demo d3;
    d3.num1 = d1.num1 + d2.num1;
    d3.num2 = d1.num2 + d2.num2;
    d3.num3 = d1.num3 + d2.num3;
    d3.printDemo();
}

/*Demo sum(Demo &d1, Demo &d2)
{
    Demo d3;
    d3.num1 = d1.num1 + d2.num1;
    d3.num2 = d1.num2 + d2.num2;
    d3.num3 = d1.num3 + d2.num3;
    return d3;
}*/

int main()
{
    Demo d1(10, 20, 30);
    Demo d2(40, 50, 60);
    //  cout << d1.num1 << d1.num2 << d1.num3 << endl;
    sum(d1, d2);
    // Demo d3 = sum(d1, d2);
    // d3.printDemo();
    return 0;
}