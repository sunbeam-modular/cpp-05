#include <iostream>
using namespace std;

int num3 = 30;

void myfun()
{
    int num1 = 10;
    static int num2 = 20;
    // local class
    class Complex
    {
    private:
        int real;
        int imag;

    public:
        Complex(int real = 10, int imag = 20)
        {
            this->real = real;
            this->imag = imag;
        }

        void printComplex()
        {
            cout << "Real = " << this->real << endl;
            cout << "Imag = " << this->imag << endl;
            // cout << "Num1 = " << num1 << endl;//NOT OK
            cout << "Num2 = " << num2 << endl; // OK
            cout << "Num3 = " << num3 << endl; //
        }
    };

    Complex c1;
    c1.printComplex();

    Complex c2(100, 200);
    c2.printComplex();
}

int main()
{
    myfun();
    return 0;
}