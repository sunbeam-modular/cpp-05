#include <iostream>
using namespace std;
class Account
{
private:
    int accno;
    string name;
    double balance;
    static int generate_accno;

public:
    Account(string name = "", double balance = 0)
    {
        this->accno = ++generate_accno;
        this->name = name;
        this->balance = balance;
    }

    int getAccno()
    {
        return this->accno;
    }
    void acceptAccount()
    {
        cout << "Enter name = ";
        cin >> this->name;
        cout << "Enter balance = ";
        cin >> this->balance;
    }
    void printAccount()
    {
        cout << "-----------------------------------" << endl;
        cout << "Accno = " << this->accno << endl;
        cout << "Name = " << this->name << endl;
        cout << "Balance = " << this->balance << endl;
        cout << "-----------------------------------" << endl;
    }
    void updateBalance(bool status)
    {
        double balance;
        cout << "Enter the amount = ";
        cin >> balance;
        if (status)
            this->balance = this->balance + balance;
        else
            this->balance = this->balance - balance;

        cout << "Your Updated balance = " << this->balance << endl;
    }
};
int Account::generate_accno = 1000;

class Bank
{
public:
    static int menu()
    {
        int choice;
        cout << "-----------------------------------" << endl;
        cout << "0.Exit" << endl;
        cout << "1.Create Account" << endl;
        cout << "2.Dispaly All Accounts" << endl;
        cout << "3.Deposit" << endl;
        cout << "4.Withdraw" << endl;
        cout << "5. Display Specific Account" << endl;
        cout << "Enter Your choice = " << endl;
        cin >> choice;
        cout << "-----------------------------------" << endl;
        return choice;
    }
    static void createAccount(Account *accounts[])
    {
        accounts[0] = new Account("Person1", 1000);
        accounts[1] = new Account("Person2", 2000);
        accounts[2] = new Account("Person3", 3000);
        accounts[3] = new Account("Person4", 4000);
        accounts[4] = new Account();
        accounts[4]->acceptAccount();
        cout << "Accounts are Created" << endl;
    }

    static int verifyAccount(Account *accounts[])
    {
        int accno;
        cout << "Enter the accno = " << endl;
        cin >> accno;
        for (int i = 0; i < 5; i++)
            if (accounts[i]->getAccno() == accno)
                return i;
        cout << "Account Does not exists.." << endl;
        return -1;
    }
};

int main()
{
    Account *accounts[5];
    int choice;
    int index;

    while ((choice = Bank::menu()) != 0)
    {
        switch (choice)
        {
        case 1:
            Bank::createAccount(accounts);
            break;
        case 2:
            for (int i = 0; i < 5; i++)
                accounts[i]->printAccount();
            break;
        case 3:
            index = Bank::verifyAccount(accounts);
            if (index > -1)
                accounts[index]->updateBalance(true);
            break;
        case 4:
            index = Bank::verifyAccount(accounts);
            if (index > -1)
                accounts[index]->updateBalance(false);
            break;
        case 5:
            index = Bank::verifyAccount(accounts);
            if (index > -1)
                accounts[index]->printAccount();
            break;
        default:
            cout << "Wrong Input :(" << endl;
            break;
        }
    }
    cout << "Thank you for using our app :) " << endl;

    // Deallocation of memory
    for (int i = 0; i < 5; i++)
        delete accounts[i];
    return 0;
}