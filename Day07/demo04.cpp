#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int imag;
    static int count;

public:
    Complex(int real, int imag)
    {
        count++;
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
        cout << "Total objects = " << count << endl;
    }

    static void printCount()
    {
        cout << "Total objects = " << count << endl;
    }
};

int Complex::count = 0; // static data member of Complex class

int main()
{
    Complex(10, 20).printComplex();
    Complex(30, 40).printComplex();
    Complex(50, 60).printComplex();
    Complex(70, 80).printComplex();
    Complex(90, 100).printComplex();

    // Complex(25, 35).printCount();// Not recommnded as count value increases
    Complex::printCount();
    return 0;
}