#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int imag;
    static int count;

public:
    Complex(int real, int imag)
    {
        this->real = real;
        this->imag = imag;
    }

    void setCount(int count)
    {
        this->count = count;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
        cout << "Count = " << this->count << endl;
    }
};

int Complex::count = 10; // static data member of Complex class

int main()
{
    Complex c1(10, 20);
    Complex c2(30, 40);
    c1.printComplex();
    c1.setCount(20);
    c2.printComplex();
    return 0;
}