#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int imag;
    static int count;

public:
    Complex(int real, int imag)
    {
        count++;
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
        cout << "Total objects = " << count << endl;
    }

    static void printCount()
    {
        cout << "Total objects = " << count << endl;
    }
};

int Complex::count = 0; // static data member of Complex class

int main()
{
    Complex c1(10, 20);
    Complex c2(10, 20);

    c1.printComplex();
    c2.printComplex();

    c1.printCount();
    Complex::printCount();
    return 0;
}