#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int imag;
    static int count;

public:
    Complex(int real, int imag)
    {
        count++;
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    void printCount()
    {
        cout << "Total objects = " << this->count << endl;
    }
};

int Complex::count = 0; // static data member of Complex class

int main()
{
    Complex c1(10, 20);
    Complex c2(30, 40);
    Complex c3(50, 60);
    Complex c4(70, 80);
    Complex c5(90, 100);
    c1.printComplex();
    c2.printComplex();
    c3.printComplex();
    c4.printComplex();
    c5.printComplex();

    c3.printCount();

    return 0;
}