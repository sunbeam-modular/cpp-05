#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int imag;
    static int count;
    static Complex *complexInstance;

    // make the ctor as private
    Complex()
    {
        count++;
        this->real = 10;
        this->imag = 20;
    }

public:
    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    static void printCount()
    {
        cout << "Total objects = " << count << endl;
    }

    static Complex *getComplexInstance()
    {
        if (complexInstance == NULL)
            complexInstance = new Complex();

        return complexInstance;
    }
};

int Complex::count = 0; // static data member of Complex class
Complex *Complex::complexInstance = NULL;
int main()
{
    Complex *cptr1 = Complex::getComplexInstance();
    Complex *cptr2 = Complex::getComplexInstance();
    Complex *cptr3 = Complex::getComplexInstance();

    Complex::printCount();
    return 0;
}