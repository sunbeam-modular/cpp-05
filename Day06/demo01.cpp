#include <iostream>
using namespace std;

void passByValue(int p_num1)
{
    cout << "Inside passByValue()" << endl;
    cout << "Value of num1 = " << p_num1 << endl;
    cout << "Address of p_num1 = " << &p_num1 << endl;
}

void passByAddress(int *p_num1)
{
    cout << "Inside passByAddress()" << endl;
    cout << "Value of num1 = " << *p_num1 << endl;
    cout << "Address of num1 = " << p_num1 << endl;
    cout << "Address of p_num1 = " << &p_num1 << endl;
}

void passByReference(int &p_num1)
{
    cout << "Inside passByReference()" << endl;
    cout << "Value of num1 = " << p_num1 << endl;
    cout << "Address of num1 = " << &p_num1 << endl;
    cout << "Address of p_num1 = " << &p_num1 << endl;
}

int main()
{
    int num1 = 10;
    cout << "using num1 :-" << endl;
    cout << "value of num1 = " << num1 << endl;
    cout << "address of num1 = " << &num1 << endl;
    // passByValue(num1);
    // passByAddress(&num1);
    passByReference(num1);

    return 0;
}