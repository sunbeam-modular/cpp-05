#include <iostream>
using namespace std;

int main()
{
    int *ptr = new int;
    *ptr = 10;

    cout << "value at dynamically allocated memory = " << *ptr << endl;
    cout << "Address of dynamically allocated memory = " << ptr << endl;
    cout << "Address of ptr = " << &ptr << endl;

    delete ptr;
    ptr = NULL;

    // cout << "Address of dynamically allocated memory = " << ptr << endl;
    // cout << "value at dynamically allocated memory = " << *ptr << endl;

    cout << "Program Finished" << endl;
    return 0;
}