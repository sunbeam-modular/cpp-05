#include <iostream>
using namespace std;

// Anonymous class
class
{
public:
    void f1()
    {
        cout << "Inside f1()" << endl;
    }
    void f2()
    {
        cout << "Inside f2()" << endl;
    }
} obj1, obj2;

int main()
{
    obj1.f1();
    obj1.f2();
    obj2.f1();
    obj2.f2();
    return 0;
}