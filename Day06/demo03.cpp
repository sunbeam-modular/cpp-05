#include <iostream>
using namespace std;

class Complex
{
private:
    int *ptrReal;
    int *ptrImag;

public:
    Complex(int real, int imag)
    {
        cout << "Inside Parameterized Ctor" << endl;
        // dynamic memory allocation
        this->ptrReal = new int;
        ptrImag = new int;

        // adding the values inside the newly allocated memory
        *this->ptrReal = real;
        *ptrImag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << *this->ptrReal << endl;
        cout << "Imag = " << *ptrImag << endl;
    }

    // Destructor
    ~Complex()
    {
        cout << "Inside Destructor destroying" << endl;
        printComplex();
        delete ptrReal;
        delete ptrImag;
        ptrReal = NULL;
        ptrImag = NULL;
    }

    // void deallocateMemory()
    // {
    //     cout << "Inside deallocation" << endl;
    //     delete ptrReal;
    //     delete ptrImag;
    //     ptrReal = NULL;
    //     ptrImag = NULL;
    // }
};

int main()
{
    Complex c1(10, 20);
    c1.printComplex();
    // c1.deallocateMemory();

    Complex c2(100, 200);
    c2.printComplex();

    return 0;
}