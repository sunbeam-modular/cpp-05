#include <iostream>
using namespace std;
class Complex
{
private:
    int real;
    int *imag;

public:
    Complex()
    {
        cout << "Inside Parameterless Ctor" << endl;
        this->real = 10;
        this->imag = new int;
        *this->imag = 20;
    }

    Complex(int real, int imag)
    {
        cout << "Inside Parameterized Ctor" << endl;
        this->real = real;
        this->imag = new int;
        *this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << *this->imag << endl;
    }

    void changeValues()
    {
        this->real = 50;
        *this->imag = 60;
    }

    ~Complex()
    {
        delete this->imag;
        this->imag = NULL;
    }
};
int main()
{
    Complex c1; // parameterless Ctor
    c1.printComplex();

    Complex c2(100, 200); // Parameterized Ctor
    c2.printComplex();

    cout << "Copying c2 object in c3" << endl;
    Complex c3 = c2; //
    c3.printComplex();

    cout << "Changing values of c3" << endl;
    c3.changeValues();
    c3.printComplex();

    cout << "Observed Changes in c2" << endl;
    c2.printComplex();
    return 0;
}