#include <iostream>
using namespace std;

class Demo
{
public:
    Demo()
    {
        cout << "Inside Parameterless Ctor" << endl;
    }
    void f1()
    {
        cout << "Inside f1()" << endl;
    }
    void f2()
    {
        cout << "Inside f2()" << endl;
    }
    ~Demo()
    {
        cout << "Inside Dtor" << endl;
    }
};

int main()
{
    // Anonymous object
    Demo().f1();
    Demo().f2();

    return 0;
}