#include <iostream>
using namespace std;
class Date
{
private:
    int day;
    int month;
    int year;

public:
    Date()
    {
        cout << "Inside Date parameterless ctor" << endl;
        this->day = 1;
        this->month = 1;
        this->year = 1990;
    }
    Date(int day, int month, int year)
    {
        cout << "Inside Date parameterized ctor" << endl;
        this->day = day;
        this->month = month;
        this->year = year;
    }
    void acceptDate()
    {
        cout << "Enter the day = ";
        cin >> this->day;
        cout << "Enter the month = ";
        cin >> this->month;
        cout << "Enter the year = ";
        cin >> this->year;
    }
    void printDate()
    {
        cout << "Date : " << this->day << "/" << this->month << "/" << this->year << endl;
    }
};

class Vehicle
{
private:
    string name;
    string vehicle_no;

public:
    Vehicle()
    {
        cout << "Inside vehicle parmeterless ctor" << endl;
        this->name = "";
        this->vehicle_no = "";
    }
    Vehicle(string name, string vehicle_no)
    {
        cout << "Inside vehicle parmeterized ctor" << endl;
        this->name = name;
        this->vehicle_no = vehicle_no;
    }
    void acceptVehicle()
    {
        cout << "Enter the vehicle name = ";
        cin >> this->name;
        cout << "Enter the vehicle number = ";
        cin >> this->vehicle_no;
    }
    void displayVehicle()
    {
        cout << "Vehicle Name = " << this->name << endl;
        cout << "Vehicle No = " << this->vehicle_no << endl;
    }
};

class Employee
{
private:
    int empid;
    string name;
    double sal;
    Date doj;
    Vehicle *veh;

public:
    Employee()
    {
        cout << "Inside employee parmeterless ctor" << endl;
        this->empid = 0;
        this->name = "";
        this->sal = 0;
        this->veh = NULL;
    }
    Employee(int empid, string name, double sal, int day, int month, int year) : doj(day, month, year)
    {
        cout << "Inside employee parmeterized ctor" << endl;
        this->empid = empid;
        this->name = name;
        this->sal = sal;
        this->veh = NULL;
    }

    Employee(int empid, string name, double sal, int day, int month, int year, string veh_name, string veh_no) : doj(day, month, year)
    {
        cout << "Inside employee parmeterized ctor" << endl;
        this->empid = empid;
        this->name = name;
        this->sal = sal;
        this->veh = new Vehicle(veh_name, veh_no);
    }
    void addVehicle()
    {
        char choice;
        cout << "Do you have a vehicle ?" << endl;
        cout << "Enter 'Y' for YES or press any key for NO" << endl;
        cin >> choice;
        if (choice == 'Y' || choice == 'y')
        {
            this->veh = new Vehicle();
            this->veh->acceptVehicle();
        }
    }
    void acceptEmployee()
    {
        cout << "Enter empid = ";
        cin >> this->empid;
        cout << "Enter name = ";
        cin >> this->name;
        cout << "Enter salary = ";
        cin >> this->sal;
        this->doj.acceptDate();
        this->addVehicle();
    }
    void displayEmployee()
    {
        cout << "Empid = " << this->empid << endl;
        cout << "Name = " << this->name << endl;
        cout << "Salary = " << this->sal << endl;
        this->doj.printDate();
        if (this->veh != NULL)
            this->veh->displayVehicle();
    }
    ~Employee()
    {
        if (this->veh != NULL)
            delete this->veh;
    }
};

int main()
{
    // Employee e1;
    // e1.acceptEmployee();
    // Employee e1(1, "rohan", 12345, 1, 5, 2000);
    // e1.addVehicle();
    Employee e1(1, "rohan", 12345, 1, 5, 2000, "Rolls", "MH1012345");
    e1.displayEmployee();
    return 0;
}