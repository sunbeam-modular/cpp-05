#include <iostream>
using namespace std;
class Date
{
private:
    int day;
    int month;
    int year;

public:
    Date()
    {
        cout << "Inside Date parameterless ctor" << endl;
        this->day = 1;
        this->month = 1;
        this->year = 1990;
    }
    Date(int day, int month, int year)
    {
        cout << "Inside Date parameterized ctor" << endl;
        this->day = day;
        this->month = month;
        this->year = year;
    }
    void acceptDate()
    {
        cout << "Enter the day = ";
        cin >> this->day;
        cout << "Enter the month = ";
        cin >> this->month;
        cout << "Enter the year = ";
        cin >> this->year;
    }
    void printDate()
    {
        cout << "Date : " << this->day << "/" << this->month << "/" << this->year << endl;
    }
};

class Employee
{
private:
    int empid;
    string name;
    double sal;
    Date doj;

public:
    Employee()
    {
        cout << "Inside employee parmeterless ctor" << endl;
        this->empid = 0;
        this->name = "";
        this->sal = 0;
    }
    Employee(int empid, string name, double sal, int day, int month, int year) : doj(day, month, year)
    {
        cout << "Inside employee parmeterized ctor" << endl;
        this->empid = empid;
        this->name = name;
        this->sal = sal;
    }
    void acceptEmployee()
    {
        cout << "Enter empid = ";
        cin >> this->empid;
        cout << "Enter name = ";
        cin >> this->name;
        cout << "Enter salary = ";
        cin >> this->sal;
        cout << "Enter the date of joining - " << endl;
        this->doj.acceptDate();
    }
    void displayEmployee()
    {
        cout << "Empid = " << this->empid << endl;
        cout << "Name = " << this->name << endl;
        cout << "Salary = " << this->sal << endl;
        this->doj.printDate();
    }
};

int main()
{

    // Employee e1;
    // e1.displayEmployee();

    // Employee e1;
    //  e1.acceptEmployee();
    //  e1.displayEmployee();

    Employee e1(1, "rohan", 12345, 1, 5, 2000);
    e1.displayEmployee();

    return 0;
}