#include <iostream>
using namespace std;
class Person // Parent OR Base class
{
private:
    string name;
    int birthYear;

public:
    Person()
    {
        cout << "Inside Person paramerterless Ctor" << endl;
        this->name = "";
        this->birthYear = 1950;
    }
    Person(string name, int birthYear)
    {
        cout << "Inside Person paramerterrized Ctor" << endl;
        this->name = name;
        this->birthYear = birthYear;
    }

    void acceptPerson()
    {
        cout << "Enter name = ";
        cin >> this->name;
        cout << "Enter birthYear = ";
        cin >> this->birthYear;
    }
    void displayPerson()
    {
        cout << "Name = " << this->name << endl;
        cout << "BirthYear = " << this->birthYear << endl;
    }
};

class Employee : Person // Child OR Derived class
{
private:
    int empid;
    double salary;
    int joinYear;

public:
    Employee()
    {
        cout << "Inside Employee paramerterless Ctor" << endl;
        this->empid = 0;
        this->salary = 0;
        this->joinYear = 1950;
    }
    Employee(int empid, double salary, int joinYear)
    {
        cout << "Inside Employee paramerterized Ctor" << endl;
        this->empid = empid;
        this->salary = salary;
        this->joinYear = joinYear;
    }
    void acceptEmployee()
    {
        acceptPerson();
        cout << "Enter empid = ";
        cin >> this->empid;
        cout << "Enter salary = ";
        cin >> this->salary;
        cout << "Enter joinYear = ";
        cin >> this->joinYear;
    }
    void displayEmployee()
    {
        this->displayPerson();
        cout << "Empid = " << this->empid << endl;
        cout << "Salary = " << this->salary << endl;
        cout << "JoinYear = " << this->joinYear << endl;
    }
};
int main()
{
    Employee e1;
    e1.acceptEmployee();
    e1.displayEmployee();
    return 0;
}