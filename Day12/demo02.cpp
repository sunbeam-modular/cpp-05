#include <iostream>
using namespace std;
class Vehicle
{
private:
    string name;
    string vehicle_no;

public:
    Vehicle()
    {
        cout << "Inside vehicle parmeterless ctor" << endl;
        this->name = "";
        this->vehicle_no = "";
    }
    Vehicle(string name, string vehicle_no)
    {
        cout << "Inside vehicle parmeterized ctor" << endl;
        this->name = name;
        this->vehicle_no = vehicle_no;
    }
    void acceptVehicle()
    {
        cout << "Enter the vehicle name = ";
        cin >> this->name;
        cout << "Enter the vehicle number = ";
        cin >> this->vehicle_no;
    }
    void displayVehicle()
    {
        cout << "Vehicle Name = " << this->name << endl;
        cout << "Vehicle No = " << this->vehicle_no << endl;
    }
};

class Employee
{
private:
    int empid;
    string name;
    double sal;
    Vehicle *veh;

public:
    Employee()
    {
        cout << "Inside employee parmeterless ctor" << endl;
        this->empid = 0;
        this->name = "";
        this->sal = 0;
        this->veh = NULL;
    }
    Employee(int empid, string name, double sal)
    {
        cout << "Inside employee parmeterized ctor" << endl;
        this->empid = empid;
        this->name = name;
        this->sal = sal;
        this->veh = NULL;
    }

    Employee(int empid, string name, double sal, string veh_name, string veh_no)
    {
        cout << "Inside employee parmeterized ctor" << endl;
        this->empid = empid;
        this->name = name;
        this->sal = sal;
        this->veh = new Vehicle(veh_name, veh_no);
    }
    void addVehicle()
    {
        char choice;
        cout << "Do you have a vehicle ?" << endl;
        cout << "Enter 'Y' for YES or press any key for NO" << endl;
        cin >> choice;
        if (choice == 'Y' || choice == 'y')
        {
            this->veh = new Vehicle();
            this->veh->acceptVehicle();
        }
    }
    void acceptEmployee()
    {
        cout << "Enter empid = ";
        cin >> this->empid;
        cout << "Enter name = ";
        cin >> this->name;
        cout << "Enter salary = ";
        cin >> this->sal;
        this->addVehicle();
    }
    void displayEmployee()
    {
        cout << "Empid = " << this->empid << endl;
        cout << "Name = " << this->name << endl;
        cout << "Salary = " << this->sal << endl;
        if (this->veh != NULL)
            this->veh->displayVehicle();
    }
    ~Employee()
    {
        if (this->veh != NULL)
            delete this->veh;
    }
};

int main()
{

    // Employee e1;
    // e1.displayEmployee();

    // Employee e1;
    // e1.acceptEmployee();
    // e1.displayEmployee();

    // Employee e1(1, "rohan", 12345);
    // e1.addVehicle();
    // e1.displayEmployee();

    Employee e1(1, "rohan", 12345, "Rolls", "MH101234");
    e1.displayEmployee();

    return 0;
}