#include <iostream>
using namespace std;
class Arithmetic
{
private:
    int num1;
    int num2;

public:
    Arithmetic(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }
    void sum()
    {
        if (this->num1 < 0 || this->num2 < 0)
            throw 101;
        cout << "Addition = " << this->num1 + this->num2 << endl;
    }

    void sub()
    {
        if (this->num1 < this->num2)
            throw 201;
        cout << "Substraction = " << this->num1 - this->num2 << endl;
    }

    void mul()
    {
        if (this->num1 <= 0 || this->num2 <= 0)
            throw 301;

        cout << "Multiplication = " << this->num1 * this->num2 << endl;
    }

    void div()
    {
        if (this->num2 <= 0)
            throw 401;
        cout << "Division = " << this->num1 / this->num2 << endl;
    }
};
int main()
{
    Arithmetic a1(20, 30);
    try
    {
        a1.sum();
        a1.sub();
        a1.mul();
        a1.div();
    }
    catch (int error)
    {
        if (error == 101)
            cout << error << ": Num1 or Num2 cannot be negative" << endl;
        else if (error == 201)
            cout << error << ": Num2 cannot be greater than num1" << endl;
        else if (error == 301)
            cout << error << ": Num1 or Num2 cannot be Zero" << endl;
        else
            cout << error << ": Num2 cannot be 0" << endl;
    }

    cout << "Program Executed Successfully" << endl;
    return 0;
}