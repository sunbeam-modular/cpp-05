#include <iostream>
using namespace std;

class ArithmeticException
{
private:
    int error_code1;
    double error_code2;

    void printDoubleError()
    {
        if (this->error_code2 == 10.01)
            cout << this->error_code2 << ": Num1 or Num2 cannot be Zero" << endl;
        else
            cout << this->error_code2 << ": Num2 cannot be 0" << endl;
    }

    void printIntError()
    {
        if (this->error_code1 == 101)
            cout << this->error_code1 << ": Num1 or Num2 cannot be negative" << endl;
        else
            cout << this->error_code1 << ": Num2 cannot be greater than num1" << endl;
    }

public:
    ArithmeticException(int error_code1)
    {
        this->error_code1 = error_code1;
        this->error_code2 = 0;
    }

    ArithmeticException(double error_code2)
    {
        this->error_code2 = error_code2;
        this->error_code1 = 0;
    }

    void printError()
    {
        if (this->error_code1 == 0)
            printDoubleError();
        else
            printIntError();
    }
};

class Arithmetic
{
private:
    int num1;
    int num2;

public:
    Arithmetic(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }
    void sum()
    {
        if (this->num1 < 0 || this->num2 < 0)
            throw ArithmeticException(101);
        cout << "Addition = " << this->num1 + this->num2 << endl;
    }

    void sub()
    {
        if (this->num1 < this->num2)
            throw ArithmeticException(201);
        cout << "Substraction = " << this->num1 - this->num2 << endl;
    }

    void mul()
    {
        if (this->num1 <= 0 || this->num2 <= 0)
            throw ArithmeticException(10.01);

        cout << "Multiplication = " << this->num1 * this->num2 << endl;
    }

    void div()
    {
        if (this->num2 <= 0)
            throw ArithmeticException(20.02);
        cout << "Division = " << this->num1 / this->num2 << endl;
    }
};
int main()
{
    Arithmetic a1(20, 0);
    try
    {
        a1.mul();
    }
    catch (ArithmeticException error)
    {
        error.printError();
    }
    cout << "Program Executed Successfully" << endl;
    return 0;
}