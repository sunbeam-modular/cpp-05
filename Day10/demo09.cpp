#include <iostream>
using namespace std;

class ArithmeticException
{
private:
    int error_code;

public:
    ArithmeticException(int error_code)
    {
        this->error_code = error_code;
    }

    void printError()
    {
        if (error_code == 101)
            cout << error_code << ": Num1 or Num2 cannot be negative" << endl;
        else if (error_code == 201)
            cout << error_code << ": Num2 cannot be greater than num1" << endl;
        else if (error_code == 301)
            cout << error_code << ": Num1 or Num2 cannot be Zero" << endl;
        else
            cout << error_code << ": Num2 cannot be 0" << endl;
    }
};

class Arithmetic
{
private:
    int num1;
    int num2;

public:
    Arithmetic(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }
    void sum()
    {
        if (this->num1 < 0 || this->num2 < 0)
            throw ArithmeticException(101);
        cout << "Addition = " << this->num1 + this->num2 << endl;
    }

    void sub()
    {
        if (this->num1 < this->num2)
            throw ArithmeticException(201);
        cout << "Substraction = " << this->num1 - this->num2 << endl;
    }

    void mul()
    {
        if (this->num1 <= 0 || this->num2 <= 0)
            throw ArithmeticException(301);

        cout << "Multiplication = " << this->num1 * this->num2 << endl;
    }

    void div()
    {
        if (this->num2 <= 0)
            throw ArithmeticException(401);
        cout << "Division = " << this->num1 / this->num2 << endl;
    }
};
int main()
{
    Arithmetic a1(20, 0);
    try
    {
        a1.mul();
    }
    catch (ArithmeticException error)
    {
        error.printError();
    }
    cout << "Program Executed Successfully" << endl;
    return 0;
}