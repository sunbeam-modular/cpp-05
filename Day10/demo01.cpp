#include <iostream>
using namespace std;
class Arithmetic
{
private:
    int num1;
    int num2;

public:
    Arithmetic(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }
    void sum()
    {
        if (this->num1 > 0 && this->num2 > 0)
            cout << "Addition = " << this->num1 + this->num2 << endl;
        else
            cout << "Num1 or Num2 cannot be negative" << endl;
    }

    void sub()
    {
        if (this->num1 > this->num2)
            cout << "Substraction = " << this->num1 - this->num2 << endl;
        else
            cout << "Num2 cannot be greater than num1" << endl;
    }

    void mul()
    {
        if (this->num1 > 0 && this->num2 > 0)
            cout << "Multiplication = " << this->num1 * this->num2 << endl;
        else
            cout << "Num1 or Num2 cannot be Zero" << endl;
    }

    void div()
    {
        if (this->num2 > 0)
            cout << "Division = " << this->num1 / this->num2 << endl;
        else
            cout << "num2 cannot be 0" << endl;
    }
};
int main()
{
    Arithmetic a1(10, 2);
    a1.div();

    return 0;
}