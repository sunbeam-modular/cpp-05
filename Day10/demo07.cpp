#include <iostream>
using namespace std;
class Arithmetic
{
private:
    int num1;
    int num2;

public:
    Arithmetic(int num1, int num2)
    {
        this->num1 = num1;
        this->num2 = num2;
    }
    void sum()
    {
        if (this->num1 < 0 || this->num2 < 0)
            throw 10;
        cout << "Addition = " << this->num1 + this->num2 << endl;
    }

    void sub()
    {
        if (this->num1 < this->num2)
            throw 2.2;
        cout << "Substraction = " << this->num1 - this->num2 << endl;
    }

    void mul()
    {
        if (this->num1 <= 0 || this->num2 <= 0)
            throw 'R';

        cout << "Multiplication = " << this->num1 * this->num2 << endl;
    }

    void div()
    {
        if (this->num2 <= 0)
            throw "div is not possible";
        cout << "Division = " << this->num1 / this->num2 << endl;
    }
};
int main()
{
    Arithmetic a1(-5, 0);
    try
    {
        a1.div();
    }
    catch (char const *error)
    {
        cout << "Num2 cannot be 0" << endl;
        try
        {
            a1.sub();
        }
        catch (double error)
        {
            cout << "Num1 should be greater than Num2 " << endl;
        }
        try
        {
            a1.sum();
        }
        catch (int error)
        {
            cout << "Num1 or Num2 cannot be negative" << endl;
        }
    }

    cout << "Program Executed Successfully" << endl;
    return 0;
}