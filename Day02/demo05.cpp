#include <iostream>

struct Employee
{
    int empid;
    char name[20];
    double salary;

    void acceptEmployee()
    {
        printf("Enter empid = ");
        scanf("%d", &empid);
        printf("Enter name = ");
        scanf("%s", &name);
        printf("Enter salary = ");
        scanf("%ld", &salary);
    }

    void printEmployee()
    {
        printf("Empid = %d\n", empid);
        printf("Name = %s\n", name);
        printf("Salary = %ld\n", salary);
    }
};

int main()
{
    struct Employee e1;
    e1.acceptEmployee();
    e1.printEmployee();
    return 0;
}