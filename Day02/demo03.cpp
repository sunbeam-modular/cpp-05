#include <iostream>

int main()
{
    wchar_t mychar = L'b';
    printf("size of wchar_t = %d\n", sizeof(mychar));
    printf("Value inside mychar = %c\n", mychar);
    return 0;
}