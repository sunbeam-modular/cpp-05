#include <stdio.h>

struct Employee
{
    int empid;
    char name[20];
    double salary;
};

void acceptEmployee(struct Employee *e1)
{
    printf("Enter empid = ");
    scanf("%d", &e1->empid);
    printf("Enter name = ");
    scanf("%s", &e1->name);
    printf("Enter salary = ");
    scanf("%ld", &e1->salary);
}

void printEmployee(struct Employee e1)
{
    printf("Empid = %d\n", e1.empid);
    printf("Name = %s\n", e1.name);
    printf("Salary = %ld\n", e1.salary);
}

int main()
{
    struct Employee e1;
    acceptEmployee(&e1);
    printEmployee(e1);

    e1.empid = 2;

    return 0;
}