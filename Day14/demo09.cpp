#include <iostream>
using namespace std;

// Abstract class
class Base
{
public:
    virtual void f1() = 0;
    // pure virtual functions
    virtual void f2() = 0;
};
class Derived : public Base
{
public:
    Derived()
    {
        cout << "Dervied :: Ctor()" << endl;
    }
    // function overriding
    void f2()
    {
        cout << "Derived :: f2()" << endl;
    }
    void f1()
    {
        cout << "Derived :: f1()" << endl;
    }
};

int main()
{
    // Base b; // Not Allowed
    //  Base *bptr = new Base(); // Not allowed
    Base *bptr = new Derived();

    bptr->f1();
    bptr->f2();

    delete bptr;
    return 0;
}