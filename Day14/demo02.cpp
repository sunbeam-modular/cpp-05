#include <iostream>
using namespace std;
class Base
{
public:
    Base()
    {
        cout << "Base :: Ctor()" << endl;
    }
    void f1()
    {
        cout << "Base :: f1()" << endl;
    }
    void f2()
    {
        cout << "Base :: f2()" << endl;
    }
};
class Derived : public Base
{
public:
    Derived()
    {
        cout << "Dervied :: Ctor()" << endl;
    }
    void f3()
    {
        cout << "Derived :: f3()" << endl;
    }
};

int main()
{
    /*Base b;
    b.f1();
    b.f2();

    Derived d;
    d.f3();
    d.f1();
    d.f2();*/

    Base *bptr = new Base();
    bptr->f1();
    bptr->f2();
    delete bptr;

    Derived *dptr = new Derived();
    dptr->f3();
    dptr->f1();
    dptr->f2();
    delete dptr;

    return 0;
}