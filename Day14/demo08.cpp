#include <iostream>
using namespace std;

// Abstract class
class Base
{
public:
    Base()
    {
        cout << "Base :: Ctor()" << endl;
    }
    void f1()
    {
        cout << "Base :: f1()" << endl;
    }
    // pure virtual functions
    virtual void f2() = 0;
};
class Derived : public Base
{
public:
    Derived()
    {
        cout << "Dervied :: Ctor()" << endl;
    }
    // function overriding
    void f2()
    {
        cout << "Derived :: f2()" << endl;
    }
    void f3()
    {
        cout << "Derived :: f3()" << endl;
    }
};

int main()
{
    // Base b;                  // Not Allowed
    // Base *bptr = new Base(); // Not allowed
    Base *bptr = new Derived();

    bptr->f1();
    bptr->f2();

    delete bptr;
    return 0;
}