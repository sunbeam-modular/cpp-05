#include <iostream>
using namespace std;
class Base
{
public:
    Base()
    {
        cout << "Base :: Ctor()" << endl;
    }
    void f1()
    {
        cout << "Base :: f1()" << endl;
    }
    void f2()
    {
        cout << "Base :: f2()" << endl;
    }
};
class Derived : public Base
{
public:
    Derived()
    {
        cout << "Dervied :: Ctor()" << endl;
    }
    void f3()
    {
        cout << "Derived :: f3()" << endl;
    }
};

int main()
{

    Base *bptr = new Derived(); // Upcasting
    bptr->f1();
    bptr->f2();
    // bptr->f3(); // object slicing

    Derived *dptr = (Derived *)bptr; // downcasting
    dptr->f3();

    delete dptr;
    return 0;
}