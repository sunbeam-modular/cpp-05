#include <iostream>
using namespace std;
class Base
{
public:
    Base()
    {
        cout << "Base :: Ctor()" << endl;
    }
    void f1()
    {
        cout << "Base :: f1()" << endl;
    }
    virtual void f2()
    {
        cout << "Base :: f2()" << endl;
    }
    virtual ~Base()
    {
        cout << "Base :: Dtor()" << endl;
    }
};
class Derived : public Base
{
public:
    Derived()
    {
        cout << "Dervied :: Ctor()" << endl;
    }
    // function overriding
    void f2()
    {
        cout << "Derived :: f2()" << endl;
    }
    void f3()
    {
        cout << "Derived :: f3()" << endl;
    }
    ~Derived()
    {
        cout << "Derived :: Dtor()" << endl;
    }
};

int main()
{
    Base *bptr = new Derived();
    // Derived *dptr = (Derived *)bptr;
    // delete dptr;
    delete bptr;
    return 0;
}