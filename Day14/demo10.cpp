#include <iostream>
using namespace std;
class Shape
{
public:
    virtual void acceptData() = 0;
    virtual void calculateArea() = 0;
};

class Circle : public Shape
{
private:
    int radius;

public:
    void acceptData()
    {
        cout << "Enter radius = ";
        cin >> this->radius;
    }
    void calculateArea()
    {
        cout << "Area of circle = " << this->radius * this->radius * 3.14 << endl;
    }
};

class Square : public Shape
{
private:
    int side;

public:
    void acceptData()
    {
        cout << "Enter side = ";
        cin >> this->side;
    }
    void calculateArea()
    {
        cout << "Area of Square = " << this->side * this->side << endl;
    }
};

class Rectangle : public Shape
{
private:
    int length;
    int breadth;

public:
    void acceptData()
    {
        cout << "Enter length and breadth = ";
        cin >> this->length >> this->breadth;
    }
    void calculateArea()
    {
        cout << "Area of rectangle = " << this->length * this->breadth << endl;
    }
};
int menu()
{
    int choice;
    cout << "0. Exit" << endl;
    cout << "1. Area of Circle" << endl;
    cout << "2. Area of Rectangle" << endl;
    cout << "3. Area of Square" << endl;
    cout << "Enter choice" << endl;
    cin >> choice;
    return choice;
}

int main()
{
    int choice;
    Shape *shape = NULL;
    while ((choice = menu()) != 0)
    {
        switch (choice)
        {
        case 1:
            shape = new Circle();
            break;
        case 2:
            shape = new Rectangle();
            break;
        case 3:
            shape = new Square();
            break;
        default:
            cout << "Wrong input .. :(" << endl;
            break;
        }
        if (shape != NULL)
        {
            shape->acceptData();
            shape->calculateArea();
            delete shape;
            shape = NULL;
        }
    }
    cout << "Thank you for using our app :)" << endl;

    return 0;
}