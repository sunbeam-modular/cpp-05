#include <iostream>
using namespace std;
// class declaration
class Complex
{
private:
    int real;
    int imag;

public:
    Complex();
    Complex(int real, int imag);
    void acceptComplex();
    void displayComplex();
};

int main()
{
    return 0;
}

// defination
Complex::Complex()
{
    this->real = 0;
    this->imag = 0;
}

Complex::Complex(int real, int imag)
{
    this->real = real;
    this->imag = imag;
}

void Complex::acceptComplex()
{
    cout << "Enter Real = ";
    cin >> this->real;
    cout << "Enter Imag = ";
    cin >> this->imag;
}

void Complex::displayComplex()
{
    cout << "Real = " << this->real << endl;
    cout << "Imag = " << this->imag << endl;
}