#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <string>
using namespace std;

class Employee
{
    int empid;
    string name;
    double salary;
    string mobile;

public:
    Employee();
    Employee(int empid);
    Employee(int empid, string name, double salary, string mobile);
    void acceptEmployee();
    void displayEmployee();
};

#endif