#include <iostream>
using namespace std;
#include "company.h"

Company::Company()
{
    this->count_of_employees = 0;
    this->choice = 1;
}
void Company::addEmployee()
{
    if (this->count_of_employees < 5)
    {
        employees_list[count_of_employees] = new Employee(count_of_employees + 1);
        employees_list[count_of_employees]->acceptEmployee();
        ++count_of_employees;
    }
    else
        cout << "Sorry we dont have new Vacencies :(" << endl;
}
void Company::displayAllEmployees()
{
    for (int i = 0; i < count_of_employees; i++)
        employees_list[i]->displayEmployee();
}

void Company::menu_list()
{
    cout << "0.Exit" << endl;
    cout << "1.Add Employee" << endl;
    cout << "2.Display All Employee" << endl;
    cout << "Enter your choice = " << endl;
    cin >> this->choice;
}

void Company::menu()
{
    while (choice != 0)
    {
        menu_list();
        switch (choice)
        {
        case 0:
            cout << "Thank You For using our app :)" << endl;
            break;
        case 1:
            addEmployee();
            break;
        case 2:
            displayAllEmployees();
            break;
        default:
            cout << "Wrong Choice Entered :(" << endl;
            break;
        }
    }
}

Company::~Company()
{
    for (int i = 0; i < count_of_employees; i++)
        delete employees_list[i];
}
