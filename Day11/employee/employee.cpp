#include <iostream>
using namespace std;
#include "employee.h"
Employee::Employee()
{
    this->empid = 0;
    this->name = "";
    this->salary = 0;
    this->mobile = "";
}

Employee::Employee(int empid)
{
    this->empid = empid;
}
Employee::Employee(int empid, string name, double salary, string mobile)
{
    this->empid = empid;
    this->name = name;
    this->salary = salary;
    this->mobile = mobile;
}

void Employee::acceptEmployee()
{
    cout << "Enter name = ";
    cin >> this->name;
    cout << "Enter salary = ";
    cin >> this->salary;
    cout << "Enter mobile = ";
    cin >> this->mobile;
}

void Employee::displayEmployee()
{
    cout << "------------------------------------" << endl;
    cout << "Empid = " << this->empid << endl;
    cout << "Name = " << this->name << endl;
    cout << "Salary = " << this->salary << endl;
    cout << "Mobile = " << this->mobile << endl;
    cout << "------------------------------------" << endl;
}
