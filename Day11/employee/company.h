#ifndef COMPANY_H
#define COMPANY_H
#include "employee.h"
class Company
{
private:
    Employee *employees_list[5];
    int count_of_employees;
    int choice;

public:
    Company();
    void addEmployee();
    void displayAllEmployees();
    void menu_list();
    void menu();
    ~Company();
};
#endif