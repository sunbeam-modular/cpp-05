#include <iostream>
using namespace std;
// declaration
void sum(int num1, int num2);

int main()
{
    sum(10, 20);
    return 0;
}

// defination
void sum(int num1, int num2)
{
    cout << "Addition = " << num1 + num2 << endl;
}
