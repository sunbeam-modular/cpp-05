#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex()
    {
        cout << "Inside parameterless ctor" << endl;
        this->real = 0;
        this->imag = 0;
    }
    Complex(int real)
    {
        cout << "Inside single parameterized ctor" << endl;
        this->real = real;
        this->imag = real;
    }

    Complex(int real, int imag)
    {
        cout << "Inside parameterized ctor" << endl;
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

int main()
{
    Complex c1;         // parameterless ctor
    Complex c2(10);     // single parameterized ctor
    Complex c3(10, 20); // parameterized ctor

    Complex c4; // Parameterless ctor
    c4 = 50;    // single parameterized ctor
    c4.printComplex();
    return 0;
};