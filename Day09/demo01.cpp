#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real = 0, int imag = 0)
    {
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    Complex sum(Complex &c2) // this-> c1
    {
        Complex result;
        result.real = this->real + c2.real;
        result.imag = this->imag + c2.imag;
        return result;
    }

    friend Complex sum(Complex &c1, Complex &c2);
};

Complex sum(Complex &c1, Complex &c2)
{
    Complex result;
    result.real = c1.real + c2.real;
    result.imag = c1.imag + c2.imag;
    return result;
}

int main()
{
    Complex c1(10, 20);
    Complex c2(30, 40);

    // int sum = 10 + 20;
    // Complex result = sum(c1, c2);
    Complex result = c1.sum(c2);
    // Complex result = c1 + c2;
    result.printComplex();
};