#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real = 0, int imag = 0)
    {
        this->real = real;
        this->imag = imag;
    }

    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    bool operator>(Complex &other)
    {
        if (this->real > other.real)
            return true;
        return false;
    }
};

// DO operator> overloading using non member function

int main()
{
    Complex c1(10, 20);
    Complex c2(30, 40);
    if (c2 > c1)
        cout << "First Object is greater";
    else
        cout << "Second Object is greater";
};