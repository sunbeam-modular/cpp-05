#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real = 0, int imag = 0)
    {
        cout << "Inside Ctor" << endl;
        this->real = real;
        this->imag = imag;
    }

    // Complex(Complex &other)
    // {
    //     cout << "Inside copy ctor" << endl;
    // }

    void setReal(int real)
    {
        this->real = real;
    }

    void acceptComplex()
    {
        cout << "Enter Real and imag Value = ";
        cin >> this->real >> this->imag;
    }
    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }

    // void operator=(Complex &other)
    // {
    //     cout << "Inside assignemnt operator" << endl;
    // }
};

int main()
{
    Complex c1(10, 20); // Parameterized Ctor
    c1.printComplex();

    Complex c2 = c1; // Copy Ctor
    c2.printComplex();

    Complex c3; // Parameterless ctor
    c3 = c1;
    c3.printComplex();
    // cout << "After change in Real " << endl;
    // c3.setReal(50);
    // c1.printComplex();
    // c3.printComplex();
    return 0;
};