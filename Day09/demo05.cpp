#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real = 0, int imag = 0)
    {
        this->real = real;
        this->imag = imag;
    }

    void acceptComplex()
    {
        cout << "Enter Real and imag Value = ";
        cin >> this->real >> this->imag;
    }
    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
    istream &operator>>(istream &in)
    {
        in >> this->real >> this->imag;
        return in;
    }
};

int main()
{
    Complex c1;
    cout << "Enter Complex Object's Real and Imag value = ";
    c1 >> cin; // c1.opertor>>(cin)
    // c1.acceptComplex();
    c1.printComplex();
    return 0;
};