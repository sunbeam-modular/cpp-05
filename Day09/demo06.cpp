#include <iostream>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real = 0, int imag = 0)
    {
        this->real = real;
        this->imag = imag;
    }

    void acceptComplex()
    {
        cout << "Enter Real and imag Value = ";
        cin >> this->real >> this->imag;
    }
    void printComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
    friend ostream &operator<<(ostream &out, Complex &ref);
};

ostream &operator<<(ostream &out, Complex &ref)
{
    out << "Real = " << ref.real << endl;
    out << "Imag = " << ref.imag << endl;
    return out;
}

int main()
{

    Complex c1;
    c1.acceptComplex();
    // c1.printComplex();
    // cin >> c1;
    cout << c1;
    return 0;
};