#include <iostream>
using namespace std;

class Employee
{
private:
    // Data Members
    int empid;
    char name[20];
    double salary;

public:
    // Member Functions
    void acceptEmployee()
    {
        cout << "Enter empid = ";
        cin >> empid;
        cout << "Enter name = ";
        cin >> name;
        cout << "Enter salary = ";
        cin >> salary;
    }

    void printEmployee()
    {
        cout << "Empid    :   " << empid << endl;
        cout << "Name    :   " << name << endl;
        cout << "Salary    :   " << salary << endl;
    }
};

int main()
{
    Employee emp; // Object
    emp.acceptEmployee();
    emp.printEmployee();

    return 0;
}