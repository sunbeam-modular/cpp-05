#include <iostream>
using namespace std;

void sum(int num1, int num2, int num3 = 0, int num4 = 0)
{
    cout << "Addition = " << num1 + num2 + num3 + num4 << endl;
}

// void button(string name = " ", string color = "Grey", int radius = 1)
// {
//     cout << "Name = " << name << endl;
//     cout << "Color = " << color << endl;
//     cout << "Radius = " << radius << endl;
// }

int main()
{
    sum(10, 20);
    sum(10, 20, 30);
    sum(10, 20, 30, 40);

    // button();
    // button("save");
    // button("cancel", "red");
    // button("login", "blue", 3);
    // button("Green");

    return 0;
}