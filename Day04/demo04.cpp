#include <iostream>
using namespace std;

class Point
{
private:
    int x;
    int y;

public:
    void accceptPoint() // accceptPoint(Point * const this)
    {
        cout << "Enter x and y values = ";
        cin >> this->x >> y;
    }
    void printPoint() // printPoint(Point * const this)
    {
        cout << "Point = (" << x << "," << this->y << ")" << endl;
    }
};

int main()
{
    Point p1;
    p1.accceptPoint(); // acceptPoint(&p1)
    p1.printPoint();

    Point p2;
    p2.accceptPoint(); // acceptPoint(&p2)
    p2.printPoint();
    return 0;
}