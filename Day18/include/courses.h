#ifndef COURSES_H
#define COURSES_H
#include <iostream>
using namespace std;
class Courses
{
private:
    int cid;
    string name;
    double fees;

public:
    Courses(int cid, string name, double fees);
    void displayCourse();
    string getName();
};
#endif