#ifndef COURSE_H
#define COURSE_H
#include <vector>
#include <iostream>
using namespace std;
#include "student.h"
#include "courses.h"

enum EmainMenu
{
    EXIT,
    SEE_ALL_COURSES,
    REGISTER,
    LOGIN

};

enum EstudentMenu
{
    TAKE_COURSE = 1,
    SEE_TAKEN_COURSES,
    LOGOUT
};

class Institute
{
private:
    EmainMenu e_mainMenu;
    EstudentMenu e_studentMenu;
    vector<Courses> coursesList;
    vector<Student *> studentsList;
    Student *student;

public:
    Institute();
    void mainMenuList();
    void mainMenu();
    void studentList();
    void studentMenu();
    bool login();
    void takeCourse();
};

#endif