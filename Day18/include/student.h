#ifndef STUDENT_H
#define STUDENT_H
#include <vector>
#include <iostream>
using namespace std;
class Student
{
private:
    int regno;
    string name;
    vector<string> taken_courses;

public:
    void acceptData();
    int getRegNo();
    string getName();
    vector<string> getTaken_Courses();
    void setTaken_Courses(string name);
};

#endif