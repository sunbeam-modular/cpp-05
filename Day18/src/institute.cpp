#include "../include/institute.h"

Institute::Institute()
{
    this->student = NULL;
    this->e_mainMenu = EmainMenu::LOGIN;
    this->e_studentMenu = EstudentMenu::TAKE_COURSE;
    this->coursesList.push_back(Courses(1, "cpp", 6500));
    this->coursesList.push_back(Courses(2, "java", 7500));
    this->coursesList.push_back(Courses(3, "python", 7500));
    this->coursesList.push_back(Courses(4, "dsa", 7000));
}
void Institute::mainMenuList()
{
    int choice;
    cout << "-------------------------------------" << endl;
    cout << "0. Exit" << endl;
    cout << "1. Display all available courses" << endl;
    cout << "2. Register" << endl;
    cout << "3. Login" << endl;
    cout << "Enter Choice = ";
    cin >> choice;
    this->e_mainMenu = EmainMenu(choice);
    cout << "-------------------------------------" << endl;
}
void Institute::mainMenu()
{
    while (this->e_mainMenu != EXIT)
    {
        this->mainMenuList();
        switch (this->e_mainMenu)
        {
        case EXIT:
            cout << "Thank you for your visiting our website :)" << endl;
            break;
        case SEE_ALL_COURSES:
            for (int i = 0; i < this->coursesList.size(); i++)
                this->coursesList[i].displayCourse();
            break;
        case REGISTER:
            student = new Student();
            student->acceptData();
            this->studentsList.push_back(student);
            student = NULL;
            break;
        case LOGIN:
            if (this->login())
                studentMenu();
            else
                cout << "Student doen not exist ..." << endl;
            break;
        default:
            cout << "Error... Some thing went wrong :(" << endl;
            break;
        }
    }
}
void Institute::studentList()
{
    int choice;
    cout << "-------------------------------------" << endl;
    cout << "1. Take a Course" << endl;
    cout << "2. See All Taken Courses" << endl;
    cout << "3. Logout" << endl;
    cout << "Enter Choice = ";
    cin >> choice;
    this->e_studentMenu = EstudentMenu(choice);
    cout << "-------------------------------------" << endl;
}
void Institute::studentMenu()
{
    cout << "Hello, Welcome " << student->getName() << endl;
    while (this->e_studentMenu != LOGOUT)
    {
        studentList();
        switch (this->e_studentMenu)
        {
        case TAKE_COURSE:
            this->takeCourse();
            break;
        case SEE_TAKEN_COURSES:
            for (int i = 0; i < this->student->getTaken_Courses().size(); i++)
                cout << i + 1 << ". " << this->student->getTaken_Courses()[i] << endl;
            break;
        case LOGOUT:
            cout << "Bye.. see you soon :)" << endl;
            student = NULL;
            break;
        default:
            cout << "Error... Some thing went wrong :(" << endl;
            break;
        }
    }
    this->e_studentMenu = EstudentMenu::TAKE_COURSE;
}

bool Institute::login()
{
    int regno;
    cout << "Enter your regno = ";
    cin >> regno;
    for (int i = 0; i < this->studentsList.size(); i++)
        if (this->studentsList[i]->getRegNo() == regno)
        {
            student = this->studentsList[i];
            return true;
        }

    return false;
}

void Institute::takeCourse()
{
    int cid;
    string cname;
    for (int i = 0; i < this->coursesList.size(); i++)
        this->coursesList[i].displayCourse();
    cout << "Enter the course id you want to take = ";
    cin >> cid;
    cname = this->coursesList[cid - 1].getName();
    cout << cname;
    student->setTaken_Courses(cname);
}