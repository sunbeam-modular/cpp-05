#include "../include/student.h"

void Student::acceptData()
{
    cout << "Enter Reg no = ";
    cin >> this->regno;
    cout << "Enter Name = ";
    cin >> this->name;
}

int Student::getRegNo()
{
    return this->regno;
}

string Student::getName()
{
    return this->name;
}
vector<string> Student::getTaken_Courses()
{
    return this->taken_courses;
}

void Student::setTaken_Courses(string name)
{
    this->taken_courses.push_back(name);
}
