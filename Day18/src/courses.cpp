#include "../include/courses.h"

Courses::Courses(int cid, string name, double fees)
{
    this->cid = cid;
    this->name = name;
    this->fees = fees;
}
void Courses::displayCourse()
{
    cout << "---------------------------" << endl;
    cout << "id = " << this->cid << endl;
    cout << "name = " << this->name << endl;
    cout << "fees = " << this->fees << endl;
    cout << "---------------------------" << endl;
}
string Courses::getName()
{
    return this->name;
}