#include <iostream>
#include <vector>
using namespace std;

class Complex
{
private:
    int real;
    int imag;

public:
    Complex(int real, int imag)
    {
        this->real = real;
        this->imag = imag;
    }

    void dispalyComplex()
    {
        cout << "Real = " << this->real << endl;
        cout << "Imag = " << this->imag << endl;
    }
};

void display(vector<Complex> v1)
{
    for (int i = 0; i < v1.size(); i++)
    {
        cout << "value at " << i << " = " << endl;
        v1[i].dispalyComplex();
    }
}

int main()
{
    vector<Complex> v1;
    v1.push_back(Complex(10, 20));
    v1.push_back(Complex(30, 40));
    v1.push_back(Complex(50, 60));
    v1.push_back(Complex(70, 80));

    display(v1);

    return 0;
}