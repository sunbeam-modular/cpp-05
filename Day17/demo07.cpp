#include <iostream>
#include <vector>
using namespace std;
void display(vector<double> v1)
{
    for (int i = 0; i < v1.size(); i++)
        cout << "value at " << i << " =" << v1[i] << endl;
}

void display(vector<char> v2)
{
    for (int i = 0; i < v2.size(); i++)
        cout << "value at " << i << " =" << v2[i] << endl;
}
int main()
{
    vector<double> v1;
    v1.push_back(10.10);
    v1.push_back(20.20);
    v1.push_back(30.40);
    v1.push_back(40.40);
    display(v1);

    vector<char> v2;
    v2.push_back('A');
    v2.push_back('B');
    v2.push_back('C');
    v2.push_back('D');
    display(v2);

    return 0;
}