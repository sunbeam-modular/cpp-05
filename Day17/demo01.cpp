#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    // cout << "Hello World" << endl;
    int num1 = 10;

    // cout << "Hex value = " << hex << num1 << endl;
    // cout << "Oct value = " << oct << num1 << endl;
    // cout << "Dec value = " << dec << num1 << endl;

    // cout << "Hex value = " << setbase(16) << num1 << endl;
    // cout << "Oct value = " << setbase(8) << num1 << endl;
    // cout << "Dec value = " << setbase(10) << num1 << endl;

    double num2 = 123.456;
    cout << "Value of num2 = " << fixed << setprecision(2) << num2 << endl;

    string name = "Hello";
    cout << setw(7) << "id = " << num1 << endl;
    cout << setw(7) << "Name = " << name << endl;

    string mobileno = "9388";
    cout << "Mobile no = " << setw(10) << mobileno << endl;
    cout << "Mobile no = " << setfill('X') << setw(10) << mobileno << endl;
};