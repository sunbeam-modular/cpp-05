#include <iostream>
#include <vector>
using namespace std;
void display(vector<int> v1)
{
    for (int i = 0; i < v1.size(); i++)
        cout << "value at " << i << " =" << v1[i] << endl;
}
int main()
{
    vector<int> v1;
    cout << "Size of vector = " << v1.size() << endl;
    v1.push_back(10);
    v1.push_back(20);
    cout << "Size of vector = " << v1.size() << endl;
    display(v1);

    return 0;
}