#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> v1;
    cout << "Size of vector = " << v1.size() << endl;
    v1.push_back(10);
    v1.push_back(20);
    cout << "Size of vector = " << v1.size() << endl;

    return 0;
}