#ifndef PERSON_H
#define PERSON_H
#include <string>
using namespace std;
class Person
{
protected:
    string name;

public:
    virtual void accept() = 0;
    virtual void display() = 0;
};
#endif