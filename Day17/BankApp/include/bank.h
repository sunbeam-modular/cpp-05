#ifndef BANK_H
#define BANK_H
#include <vector>
#include "../include/person.h"
class Bank
{
private:
    int choice;
    vector<Person *> database;
    void menuList();
    void display(const type_info &type);
    void update_balance(bool status);

public:
    Bank();
    void menu();
};

#endif