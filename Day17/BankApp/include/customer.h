#ifndef CUSTOMER_H
#define CUSTOMER_H
#include "person.h"
class Customer : public Person
{
private:
    int accno;
    double balance;

public:
    void accept();
    void display();
    void deposit(double amount);
    void withdraw(double amount);
    int getAccNo();
};
#endif