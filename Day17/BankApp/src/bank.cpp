#include <iostream>
using namespace std;
#include "../include/bank.h"
#include "../include/employee.h"
#include "../include/customer.h"

Bank::Bank()
{
    this->choice = 1;
}

void Bank::display(const type_info &type)
{
    for (int i = 0; i < this->database.size(); i++)
        if (typeid(*this->database[i]) == type)
            this->database[i]->display();
}

void Bank::update_balance(bool status)
{
    int accno;
    double amount;

    cout << "Enter accno = ";
    cin >> accno;

    for (int i = 0; i < this->database.size(); i++)
        if (typeid(*this->database[i]) == typeid(Customer))
        {
            // downcasting to call the inspector getacc() from customer class
            Customer *cust = dynamic_cast<Customer *>(this->database[i]);
            if (cust->getAccNo() == accno)
            {
                cout << "Enter the amount = ";
                cin >> amount;
                if (status)
                    cust->deposit(amount);
                else
                    cust->withdraw(amount);
                break;
            }
        }
}

void Bank::menuList()
{
    cout << "-------------------------------" << endl;
    cout << "0.Exit" << endl;
    cout << "1.Add Employee" << endl;
    cout << "2.Add Customer" << endl;
    cout << "3.Dispaly All Employees" << endl;
    cout << "4.Display All Customers" << endl;
    cout << "5.Deposit" << endl;
    cout << "6.Withdraw" << endl;
    cout << "Enter your Choice = ";
    cin >> this->choice;
    cout << "-------------------------------" << endl;
}
void Bank::menu()
{
    Person *p = NULL;
    while (this->choice != 0)
    {
        this->menuList();
        switch (this->choice)
        {
        case 0:
            cout << "Thank you for banking with us :)" << endl;
            break;
        case 1:
            p = new Employee();
            p->accept();
            this->database.push_back(p);
            break;
        case 2:
            p = new Customer();
            p->accept();
            this->database.push_back(p);
            break;
        case 3:
            this->display(typeid(Employee));
            break;
        case 4:
            this->display(typeid(Customer));
            break;
        case 5:
            update_balance(true);
            break;
        case 6:
            update_balance(false);
            break;

        default:
            cout << "Wrong choice :(" << endl;
            break;
        }
    }
}
