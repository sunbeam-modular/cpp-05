#include <iostream>
using namespace std;
#include "../include/employee.h"

void Employee::accept()
{
    cout << "Enter name = ";
    cin >> this->name;
    cout << "Enter empid = ";
    cin >> this->empid;
    cout << "Enter salary = ";
    cin >> this->salary;
}
void Employee::display()
{
    cout << "-------------------------------" << endl;
    cout << "Empid = " << this->empid << endl;
    cout << "Name = " << this->name << endl;
    cout << "Salary = " << this->salary << endl;
    cout << "-------------------------------" << endl;
}
