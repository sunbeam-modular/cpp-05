#include <iostream>
using namespace std;
#include "../include/customer.h"

void Customer::accept()
{
    cout << "Enter name = ";
    cin >> this->name;
    cout << "Enter accno = ";
    cin >> this->accno;
    cout << "Enter balance = ";
    cin >> this->balance;
}
void Customer::display()
{
    cout << "-------------------------------" << endl;
    cout << "Accno = " << this->accno << endl;
    cout << "Name = " << this->name << endl;
    cout << "balance = " << this->balance << endl;
    cout << "-------------------------------" << endl;
}

int Customer::getAccNo()
{
    return this->accno;
}

void Customer::deposit(double amount)
{
    this->balance = this->balance + amount;
    cout << "Deposit Sucessful :)" << endl;
}
void Customer::withdraw(double amount)
{
    if (this->balance < amount)
        cout << "balance is insufficient :(" << endl;
    else
        this->balance = this->balance - amount;
}
